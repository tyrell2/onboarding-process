Welcome to your Onboarding Issue. 

Issue is not an issue. It's just a name called in Gitlab for a specific task. 

We are so excited that you have arrived at Magestore. As a new Magestore team member (except for Sales team), you will use Gitlab as part of your job. If you come from a non technical background, this might be new for you. At times the onboarding issue might feel overwhelming, but if you have any questions along the way, please feel free to ask your Team, your Buddy or MageX. Refer to the [Handbook Onboarding](https://handbook.magestore.com/books/onboarding-newbie) for mass knowledge. You don't need to remember all the checklist, just copy it to your account and tick check along the way on your onboarding process. 

Copy this issue to create an Onboarding Issue for you weekly or monthly (by click the icon Display source code next to Edit button of this template). Share it with your team mates. 

-------------
### 1ST WEEK BEFORE STARTING DAY 
* [ ] Choose an English Name & inform the Recruiter 
* [ ] Fill in form to get all essential information of Newbie 
* [ ] Take a video to self introduce yourself to all Magestore members. Less than 1 minutes. Send it to your Recruiter & Team 
* [ ] Read Handbook [Mission - Vision - Core Values](https://handbook.magestore.com/books/mission---vision---core-values)
* [ ] Watch Magestore Core Values via Video [Magestore Core Values](https://www.youtube.com/watch?v=v6cFU7hVPs8) by Steve - CEO 
* [ ] Learn Magestore origin - Watch video about [Magestore History](https://www.youtube.com/watch?v=QL55Clcf_6o) by Steve (CEO) 
* [ ] Learn [Agile Manifesto](https://handbook.magestore.com/books/agile-mindset-tools/page/agile-manifesto)
* [ ] Read [Agile Manifesto explaination](https://insights.magestore.com/posts/agile-manifesto-la-gi-va-y-nghia) via Insights.magestore.com
* [ ] Learn Basic Scrum via the post [post](https://career.magestore.com/post/scrum)
* [ ] Learn Srcum Events with [Sprint Planning](https://career.magestore.com/post/sprint-planning), [Daily Standup Meeting](https://career.magestore.com/post/daily-standup), [Sprint Review](https://career.magestore.com/post/sprint-review), [Sprint Retro](https://career.magestore.com/post/sprint-retrospective)


### 1ST WEEK FROM DAY 0 
* [ ] Register Learning Hub @Magestore and learn the course ["Role in Scrum"](http://learning.magestore.com/khoa-hoc/roles-in-a-scrum-team)
* [ ] Join Onboarding call with MageX member 
* [ ] Contribute 1 Article to Stories (any topic that you like the most)
* [ ] Submit your daily work as check in successfully 
* [ ] Give your first Kudos to those who you want to say thank the most 

### 2ND WEEK FROM DAY 0 
* [ ] Join `magestore-buddies` channel and get paired with Magestorer. Tell your group & experience as comment in this issue
* [ ] Give kudos to a member in different team to say thanks about something he/she did for you recently 
* [ ] Invite your team for a hangout this week and bonding with them or at least you can book a date!
* [ ] Read [Remote Working Mindset](https://handbook.magestore.com/books/working-remote-guide/chapter/remote-working-mindset) by Steve 
* [ ] Join Onboarding call with MageX member, share your 1st win 1st month and your work plan

### 3RD WEEK FROM DAY 0 
* [ ] Continue another Buddy call with Magestorer to make friends with more people 
* [ ] Be a faciliator for one Scrum event in team (which can by Daily/Retro/Review/Planning) 
* [ ] Do the test about Agile & Scrum and get 80% passed via this [course](http://learning.magestore.com/khoa-hoc/test-agile-scum-knowledge)
* [ ] Join Onboarding call with MageX member 

### LAST WEEK IN 1ST MONTH 
* [ ] Be representative of Team to show off Team Result & Plan in Monday Motivation 
* [ ] Continue another Buddy call with Magestorer to make friends with more people
* [ ] Be a faciliator for one Scrum event in team (which can by Daily/Retro/Review/Planning) which is different from the last week 
* [ ] Join Onboarding call with MageX member 
* [ ] Follow [Handbook Guideline](https://handbook.magestore.com/books/magestore-handbook) and contribute your improvements for Onboarding experience via [this book](https://handbook.magestore.com/books/onboarding-newbie)
* 